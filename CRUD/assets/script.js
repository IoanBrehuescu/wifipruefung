

const updateHandler = () => {

    let name = document.querySelector('#name');
    let number = document.querySelector('#number')

    fetch('/update', {
        method: "PATCH",
        //PATCH, POST, PUT, DELETE, GET
        header: `content-type: application/json`,
        body: `name=${name}&number=${number}`

    }).then(resp => resp.json()) 
    .then(data => {
        console.log(data);
    })
}

const getHandler = () => {
    
    fetch('/update', {
        method: "GET"

    }).then(resp => resp.json()) 
    .then(data => {
        console.log(data);
    })
}