// 1. npm init

//2. Starten 

// npm start


// MAIN-Process
// von dort wird RENDERER-Process (Browser-Fenster) gestartet

// IPC => Kommunikation zwischen MAin und Renderer
// im Main wird ipcMain benötigt, im Renderer ipcRenderer
// Kommunikation ist eventdriven !

const {app, BrowserWindow, screen, Menu, shell, ipcMain} = require( 'electron' );

let fenster;

const appMenu = [
    { label:'Meine App',submenu:[
        {
            label:'Beenden', 
            click:()=>{
                app.quit();
            }, 
            accelerator:"CmdOrCtrl + Q + w"
        },
        {label:'DevTools', role:'toggleDevTools'},  
        {label:'Beep', click: shell.beep, accelerator:"CmdOrCtrl + Shift + j"}
    ]},
    { label:'Funktionen',submenu:[
        {label:'Startseite', click:()=>{
            fenster.loadFile( 'view/index.html' );
        }},
        {label:'Seite2', click:()=>{
            fenster.loadFile( 'view/seite2.html' );
        }},
        {label:'WIFI Wien', click:()=>{
            shell.openExternal( 'http://wifiwien.at' );
        }}
    ]}
];


const starteApplikation = ()=>{

    let displays = screen.getAllDisplays(); // Array mit allen Bildschirmen
    console.log( displays );
    let targetArea = displays[displays.length-1].workArea;
    // Aufgabe: Fenster öffnet am "letzten" Bildschirm mit Breite 400 und voller Höhe am rechten Rand
    const WINWIDTH = 400;

    fenster = new BrowserWindow({
        width:WINWIDTH,
        height:targetArea.height,
        x:targetArea.x+targetArea.width-WINWIDTH,
        y:targetArea.y,
        resizable: false,
        movable: false,
        webPreferences:{
            devTools: true,
            nodeIntegration: true, // geht, macht man nicht!
            contextIsolation: false
        },
        icon: __dirname + '/assets/icon_128.png'
        /*frame: false,
        transparent: true,
        alwaysOnTop: true*/
    });

    fenster.loadFile( 'view/index.html' );
    fenster.webContents.openDevTools(); // öffnet DevTool sofort

    // alle Renderer haben appMenu
    Menu.setApplicationMenu( Menu.buildFromTemplate( appMenu ) );

}

// alt: app.on( 'ready', starteApplikation );
app.whenReady().then( starteApplikation );

//- MacOS
app.on( 'window-all-closed', ()=>{
    app.quit(); // Beende Applikation
});
//+ MacOS

// Main "horcht" (=listen) auf 'ende'
ipcMain.on( 'ende', ()=>{
    app.quit();
})