const {shell, ipcRenderer} = require( 'electron' ); // require nur wenn nodejs

document.querySelector( '#extern' ).onclick = (e)=>{
    e.preventDefault(); // !!! sonst wird Link in App geöffnet (zusätzlich)
    shell.openExternal( e.target.href );
}

document.querySelector( 'button' ).onclick = ()=>{
    ipcRenderer.send( 'ende' );
}