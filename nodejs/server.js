// 1. npm init -y
// 2. npm i express, fs, 

"scripts":
 {
    "mon": "nodemon server.js",
    "start": "node server.js"
  }

const express = require('express');
const fs = require('fs');

// Wenn mit CSV arbeiten
const createCsvWriter = require('csv-writer').createObjectCsvWriter
const { parse } = require('csv-parse')



const app = express();
// 3. const app = express();
const PORT = 9003;
const server = app.listen( PORT, ()=>{
    console.log( `Server http://localhost:${PORT}` );
});


// Middleware
app.use(express.static('public'));
app.use(express.urlencoded({ extended: false })); // x-www-form-url-encoded
app.use(express.json() );  // application/json

// Custom middleware for CSV parsing
app.use(upload.single('file'), (req, res, next) => {
  if (req.file && req.file.mimetype === 'text/csv') {
    const results = [];
    csv.parseString(req.file.buffer.toString(), { headers: true })
      .on('data', (data) => results.push(data))
      .on('end', () => {
        req.body = results;
        next();
      });
  } else {
    next();
  }
});

// CSV einlesen

const csvWriter = createCsvWriter({
    path: USERFILE,
    header: [
        { id: 'id', title: 'ID' },
        { id: 'email', title: 'Email' },
        { id: 'ch', title: 'CH' }
    ]
});


// zum beschreiben von CSV 
await csvWriter.writeRecords([{
    id: uuidv4(),
    email: email,
    ch: acceptTerms
}]);


//Asssd