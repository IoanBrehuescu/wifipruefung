// neuen ServiceWorker einrichten
if ( navigator.serviceWorker ) {
    navigator.serviceWorker.register( '/serviceWorker.js' )
    .then( ()=>{ console.log( 'ServiceWorker registered.'); })
    .catch( (err)=>{ console.log( 'ServiceWorker Error.', err); });
}


// User ein Install-Prompt anzeigen
window.addEventListener( 'beforeinstallprompt', (e)=>{
    console.log( 'beforeinstallprompt' );
    e.userChoice.then( res => {
        if ( res.outcome == 'dismissed' ) {
            console.log( 'User nicht installiert.' );
        } else {
            console.log( 'PWA installiert.' );
        }
    });
});